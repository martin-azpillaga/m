const { library, categories } = require("../language/standard_library");

let scope;
let created_entities;
let roles;
let components;
let material_properties;
let interpolators;

function infer_implicit_information (tree)
{
	components = {};

	for (const program of tree.programs || [])
	{
		if (program.syntactic_type === "system")
		{
			cpu_system(program)
		}
		else
		{
			gpu_system(program)
		}
	}

	tree.components = components;
}
function cpu_system (system)
{
	scope = [[]];
	created_entities = [[]];
	roles = {};

	(system.statements || []).forEach(statement);

	system.roles = roles;
}
function statement (statement)
{
	switch (statement.syntactic_type)
	{
		case "iteration": iteration(statement);
		break; case "selection": selection(statement);
		break; case "assignment": assignment(statement);
	}
}
function pop_scope ()
{
	scope.pop();
	created_entities.pop();
}
function push_scope ()
{
	scope.push([...scope[scope.length-1]])
	created_entities.push([...created_entities[created_entities.length-1]]);
}
function declare (name)
{
	scope[scope.length-1].push(name);
}
function is_declared (name)
{
	return scope[scope.length-1].includes(name);
}
function create (name)
{
	created_entities[created_entities.length-1].push(name);
}
function is_created (name)
{
	return created_entities[created_entities.length-1].includes(name);
}
function iteration (iteration)
{
	push_scope();

	declare(iteration.iterator.text);

	if (iteration.collection)
	{
		expression(iteration.collection);
	}

	(iteration.statements || []).forEach(statement);

	pop_scope();
}
function selection (selection)
{
	push_scope();

	expression(selection.condition);

	(selection.statements || []).forEach(statement);

	pop_scope();
}
function assignment (assignment)
{
	if (assignment.address.components)
	{
		address(assignment.address);
		expression(assignment.expression);
	}
	else
	{
		if ( ! is_declared(assignment.address.root))
		{
			declare(assignment.address.root.text);

			assignment.address.declaration = true;
		}

		if (assignment.expression.syntactic_type === "functional" && assignment.expression.name.text == "create")
		{
			create(assignment.address.root.text);

			assignment.address.created_entity = true;
		}

		expression(assignment.expression);
	}
}
function expression (expression)
{
	switch (expression.syntactic_type)
	{
		case "priority": priority(expression);
		break; case "binary": binary(expression);
		break; case "unary": unary(expression);
		break; case "functional": functional(expression);
		break; case "address": address(expression);
	}
}
function priority (priority)
{
	expression(priority.expression);
}
function unary (unary)
{
	if (unary.operator.text == "#")
	{
		return indent`${expression(unary.expression)}.Count`;
	}
	return indent`${unary_operator(unary.operator.text)} ${expression(unary.expression)}`;
}
function binary (binary)
{
	expression(binary.left);
	expression(binary.right);
}
function functional (functional)
{
	(functional.arguments || []).forEach(expression);
}
function address (address)
{
	for (const component of address.components || [])
	{
		if ( ! library[categories.component][component.text])
		{
			components[component.text] = component.semantic_type;
		}
	}

	if (is_created(address.root.text))
	{
		address.created_entity = true;
	}
	else if (address.components)
	{
		roles[address.root.text] = roles[address.root.text] || new Set();
		roles[address.root.text].add(address.components[0].text);
	}
}

function gpu_system(system)
{
	material_properties = {};
	interpolators = { position: false }

	for (const assignment of system.statements || [])
	{
		gpu_assignment(assignment);
	}

	system.properties = material_properties;
	system.interpolators = interpolators;
	system.lit = false;
}
function gpu_assignment(assignment)
{
	gpu_expression(assignment.expression);
}
function gpu_expression(expression)
{
	switch (expression.syntactic_type)
	{
		case "gpu_priority": gpu_expression(expression.expression);
		break; case "gpu_binary": gpu_expression(expression.left); gpu_expression(expression.right)
		break; case "gpu_unary": gpu_expression(expression.expression);
		break; case "gpu_functional": expression.arguments.forEach(gpu_expression);
		break; case "gpu_material_address": material_address(expression);
		break; case "gpu_fragment_address": fragment_address(expression);
	}
}
function material_address(address)
{
	material_properties[address.component.text] = address.semantic_type;
}
function fragment_address(address)
{
	if (address.parent.syntactic_type === "gpu_assignment" && address.parent.address === address)
	{
		interpolators[address.component.text] = true;
	}
	else
	{
		interpolators[address.component.text] = interpolators[address.component.text] || false;
	}
}
module.exports = { infer_implicit_information };
